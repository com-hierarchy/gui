<?php

Route::get('/',	function(){
	return redirect()->route('setup.index');
});

Route::group([
	'as'		=> 'setup.',
	'prefix' 	=> 'setup'], function() {

	Route::get('/', ['uses' => 'SetupControl@index', 'as' => 'index']);
});